jQuery(document).ready(function() {
   $('#genji').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'bottom auto',
      html: true,
      content: function() {
        return $('#content-genji').html();
      }
  });
   $('#mccree').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'bottom auto',
      html: true,
      content: function() {
        return $('#content-mccree').html();
      }
  });
   $('#pharah').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'bottom auto',
      html: true,
      content: function() {
        return $('#content-pharah').html();
      }
  });
   $('#reaper').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'bottom auto',
      html: true,
      content: function() {
        return $('#content-reaper').html();
      }
  });
   $('#soldier76').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'bottom auto',
      html: true,
      content: function() {
        return $('#content-soldier76').html();
      }
  });
   $('#sombra').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'bottom auto',
      html: true,
      content: function() {
        return $('#content-sombra').html();
      }
  });
   $('#tracer').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'bottom auto',
      html: true,
      content: function() {
        return $('#content-tracer').html();
      }
  });
   $('#liao').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'bottom auto',
      html: true,
      content: function() {
        return $('#content-liao').html();
      }
  });
   $('#glitch').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'bottom auto',
      html: true,
      content: function() {
        return $('#content-glitch').html();
      }
  });
   $('#hellfire').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'bottom auto',
      html: true,
      content: function() {
        return $('#content-hellfire').html();
      }
  });
   $('#bastion').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'right',
      html: true,
      content: function() {
        return $('#content-bastion').html();
      }
  });
   $('#hanzo').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'right',
      html: true,
      content: function() {
        return $('#content-hanzo').html();
      }
  });
   $('#junkrat').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'right',
      html: true,
      content: function() {
        return $('#content-junkrat').html();
      }
  });
   $('#mei').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'right',
      html: true,
      content: function() {
        return $('#content-mei').html();
      }
  });
   $('#torbjorn').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'right',
      html: true,
      content: function() {
        return $('#content-torbjorn').html();
      }
  });
   $('#widowmaker').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'right',
      html: true,
      content: function() {
        return $('#content-widowmaker').html();
      }
  });
   $('#athena').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'left',
      html: true,
      content: function() {
        return $('#content-athena').html();
      }
  });
   $('#fusionator').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'left',
      html: true,
      content: function() {
        return $('#content-fusionator').html();
      }
  });
   $('#dva').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'right',
      html: true,
      content: function() {
        return $('#content-dva').html();
      }
  });
   $('#orisa').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'right',
      html: true,
      content: function() {
        return $('#content-orisa').html();
      }
  });
   $('#reinhardt').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'right',
      html: true,
      content: function() {
        return $('#content-reinhardt').html();
      }
  });
   $('#roadhog').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'right',
      html: true,
      content: function() {
        return $('#content-roadhog').html();
      }
  });
   $('#winston').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'right',
      html: true,
      content: function() {
        return $('#content-winston').html();
      }
  });
   $('#zarya').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'right',
      html: true,
      content: function() {
        return $('#content-zarya').html();
      }
  });
   $('#soundquake').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'right auto',
      html: true,
      content: function() {
        return $('#content-soundquake').html();
      }
  });
   $('#doomfist').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'top auto',
      html: true,
      content: function() {
        return $('#content-doomfist').html();
      }
  });
   $('#ana').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'top auto',
      html: true,
      content: function() {
        return $('#content-ana').html();
      }
  });
   $('#lucio').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'top auto',
      html: true,
      content: function() {
        return $('#content-lucio').html();
      }
  });
   $('#mercy').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'top auto',
      html: true,
      content: function() {
        return $('#content-mercy').html();
      }
  });
   $('#symmetra').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'top auto',
      html: true,
      content: function() {
        return $('#content-symmetra').html();
      }
  });
   $('#zenyatta').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'top auto',
      html: true,
      content: function() {
        return $('#content-zenyatta').html();
      }
  });
   $('#cardinal').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'top auto',
      html: true,
      content: function() {
        return $('#content-cardinal').html();
      }
  });
   $('#flags').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'top auto',
      html: true,
      content: function() {
        return $('#content-flags').html();
      }
  });
   $('#talyx').popover({
      toggle:'popover',
      container:'body',
      trigger:'hover click',
      placement:'top auto',
      html: true,
      content: function() {
        return $('#content-talyx').html();
      }
  });
});
